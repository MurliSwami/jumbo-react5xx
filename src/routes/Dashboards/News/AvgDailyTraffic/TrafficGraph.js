import React from 'react';
import { Area, AreaChart, ResponsiveContainer, Tooltip } from 'recharts';
import Box from '@material-ui/core/Box';
import { news } from '../../../../@fake-db/dashboards/news';

const ArticlesGraph = () => {
  const { dailyTraffic } = news;
  return (
    <ResponsiveContainer width="100%" height={80}>
      <AreaChart data={dailyTraffic} margin={{ top: 0, right: 0, left: 0, bottom: 0 }}>
        <Tooltip
          labelStyle={{ color: 'black' }}
          cursor={false}
          content={data => {
            return data.payload[0] ? (
              <Box component="span" p={4} color="#fff">
                {data.payload[0].payload.count}
              </Box>
            ) : null;
          }}
          wrapperStyle={{
            background: '#0795F4',
            borderRadius: 10,
            radius: 10,
            overflow: 'hidden',
          }}
        />
        <Area dataKey="count" strokeWidth={3} stackId="2" stroke="#FFf" fill="#41AEF7" fillOpacity={1} />
      </AreaChart>
    </ResponsiveContainer>
  );
};

export default ArticlesGraph;
