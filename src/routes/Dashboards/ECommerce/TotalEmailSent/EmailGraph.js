import React from 'react';
import { Line, LineChart, ResponsiveContainer, Tooltip } from 'recharts';
import Box from '@material-ui/core/Box';
import { eCommerce } from '../../../../@fake-db';

const EmailGraph = () => {
  const { totalEmails } = eCommerce;
  return (
    <ResponsiveContainer width="100%" height={120}>
      <LineChart data={totalEmails} margin={{ top: 0, right: 0, left: 0, bottom: 0 }}>
        <Tooltip
          labelStyle={{ color: 'black' }}
          cursor={false}
          content={data => {
            return data.payload[0] ? (
              <Box p={4} color="#fff">
                <Box>Sent - {data.payload[0].payload.sent}</Box>
                <Box mt={1}>Bounced - {data.payload[0].payload.bounced}</Box>
              </Box>
            ) : null;
          }}
          wrapperStyle={{
            background: '#000',
            borderRadius: 10,
            radius: 10,
            overflow: 'hidden',
          }}
        />
        <Line dataKey="sent" type="monotone" dot={null} strokeWidth={3} stackId="2" stroke="#FFA800" />
        <Line dataKey="bounced" type="monotone" dot={null} strokeWidth={3} stackId="2" stroke="#F3E5CF" />
      </LineChart>
    </ResponsiveContainer>
  );
};

export default EmailGraph;
