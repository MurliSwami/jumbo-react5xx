import React from 'react';
import { Line, LineChart, ResponsiveContainer, Tooltip } from 'recharts';
import Box from '@material-ui/core/Box';
import { eCommerce } from '../../../../@fake-db';

const SaleGraph = () => {
  const { lastMonthSaleStats } = eCommerce;
  return (
    <ResponsiveContainer width="100%" height={120}>
      <LineChart data={lastMonthSaleStats} margin={{ top: 0, right: 0, left: 0, bottom: 0 }}>
        <Tooltip
          labelStyle={{ color: 'black' }}
          cursor={false}
          content={data => {
            return data.payload[0] ? (
              <Box component="span" p={4} color="#fff">
                ${data.payload[0].payload.value}
              </Box>
            ) : null;
          }}
          wrapperStyle={{
            background: '#000',
            borderRadius: 10,
            radius: 10,
            overflow: 'hidden',
          }}
        />
        <Line dataKey="value" type="monotone" dot={null} strokeWidth={3} stackId="2" stroke="#4200FF" />
      </LineChart>
    </ResponsiveContainer>
  );
};

export default SaleGraph;
