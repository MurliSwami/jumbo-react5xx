import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import AvatarGroup from '@material-ui/lab/AvatarGroup';

export default function GroupAvatars() {
  return (
    <AvatarGroup max={4}>
      <Avatar alt="Remy Sharp" src={'/images/avatar/avatar-1.png'} />
      <Avatar alt="Travis Howard" src={'/images/avatar/avatar-2.png'} />
      <Avatar alt="Cindy Baker" src={'/images/avatar/avatar-1.png'} />
      <Avatar alt="Agnes Walker" src={'/images/avatar/avatar-2.png'} />
      <Avatar alt="Trevor Henderson" src={'/images/avatar/avatar-1.png'} />
    </AvatarGroup>
  );
}
