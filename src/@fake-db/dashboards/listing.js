import Box from '@material-ui/core/Box';
import React from 'react';
import { getCustomDateTime } from '../../@jumbo/utils/dateHelper';

export const statisticsGraphData = [
  { month: 'Jan', price: 200 },
  { month: 'Feb', price: 300 },
  { month: 'Mar', price: 550 },
  { month: 'Apr', price: 500 },
  { month: 'May', price: 700 },
  { month: 'Jun', price: 450 },
  { month: 'Jul', price: 770 },
  { month: 'Aug', price: 900 },
];

export const dealsAnalyticsData = [
  { month: 'Jan', queries: 400, deals: 400 },
  { month: 'Feb', queries: 500, deals: 600 },
  { month: 'Mar', queries: 400, deals: 300 },
  { month: 'Apr', queries: 350, deals: 200 },
  { month: 'May', queries: 700, deals: 700 },
  { month: 'Jun', queries: 100, deals: 600 },
  { month: 'Jul', queries: 500, deals: 50 },
  { month: 'Aug', queries: 350, deals: 550 },
  { month: 'Sep', queries: 300, deals: 200 },
  { month: 'Oct', queries: 200, deals: 500 },
  { month: 'Nov', queries: 200, deals: 600 },
  { month: 'Dec', queries: 200, deals: 100 },
];

export const popularAgents = [
  {
    id: 1,
    name: 'Albert Hall',
    deals: 23,
    profilePic: '/images/avatar/avatar11.jpg',
    rating: 3.5,
    profileCompleted: 10,
  },
  {
    id: 2,
    name: 'John Hall',
    deals: 20,
    profilePic: '/images/avatar/avatar12.jpg',
    rating: 4.5,
    profileCompleted: 10,
  },
  {
    id: 3,
    name: 'Jackson Hall',
    deals: 21,
    profilePic: '/images/avatar/avatar3.jpg',
    rating: 3.5,
    profileCompleted: 10,
  },
  {
    id: 4,
    name: 'Jonty Hall',
    deals: 22,
    profilePic: '/images/avatar/avatar4.jpg',
    rating: 4.5,
    profileCompleted: 10,
  },
  {
    id: 5,
    name: 'Jonathan Hall',
    deals: 23,
    profilePic: '/images/avatar/avatar5.jpg',
    rating: 3.5,
    profileCompleted: 10,
  },
  {
    id: 6,
    name: 'Shane Hall',
    deals: 24,
    profilePic: '/images/avatar/avatar6.jpg',
    rating: 4.5,
    profileCompleted: 10,
  },
  {
    id: 7,
    name: 'Lisa Hall',
    deals: 25,
    profilePic: '/images/avatar/avatar7.jpg',
    rating: 3.5,
    profileCompleted: 10,
  },
  {
    id: 8,
    name: 'Cheeni Hall',
    deals: 26,
    profilePic: '/images/avatar/avatar8.jpg',
    rating: 4.5,
    profileCompleted: 10,
  },
  {
    id: 9,
    name: 'Chilbram Hall',
    deals: 27,
    profilePic: '/images/avatar/avatar9.jpg',
    rating: 3.5,
    profileCompleted: 10,
  },
  {
    id: 10,
    name: 'Danny Hall',
    deals: 83,
    profilePic: '/images/avatar/avatar10.jpg',
    rating: 4.5,
    profileCompleted: 10,
  },
];

export const propertyTabCategories = [
  { name: 'New Jersey', slug: 'new_jersey' },
  { name: 'Colorado', slug: 'colorado' },
  { name: 'Albama', slug: 'albama' },
];

export const propertiesList = [
  {
    id: 1,
    images: [
      {
        image: '/images/properties/bedroom-1.jpeg',
        title: 'image 1',
      },
      {
        image: '/images/properties/bedroom-2.jpeg',
        title: 'image 2',
      },
      {
        image: '/images/properties/bedroom-3.jpeg',
        title: 'image 3',
      },
    ],
    title: 'Luxury family home at beach side',
    address: '2972, Washington Road, New Jersey',
    bedrooms: 3,
    bathrooms: 3,
    area: '1400 m2',
    owner: { id: 1, name: 'John Nash' },
    publishedDate: 'June 26, 2020',
    availability: 'sale',
    isTrending: true,
    price: '$670,500',
    pricePerSqFt: '$587/sqft',
    category: 'new_jersey',
  },
  {
    id: 2,
    images: [
      {
        image: '/images/properties/bedroom-4.jpeg',
        title: 'image 1',
      },
      {
        image: '/images/properties/bedroom-5.jpeg',
        title: 'image 2',
      },
      {
        image: '/images/properties/bedroom-6.jpeg',
        title: 'image 3',
      },
    ],
    title: 'Sunset view Apartment in Colarado',
    address: '2972, Washington Road, New Jersey',
    bedrooms: 3,
    bathrooms: 3,
    area: '1400 m2',
    owner: { id: 1, name: 'John Nash' },
    publishedDate: 'June 25, 2020',
    availability: 'rent',
    isTrending: false,
    price: '$670,500',
    pricePerSqFt: '$587/sqft',
    category: 'colorado',
  },
  {
    id: 3,
    images: [
      {
        image: '/images/properties/bedroom-7.jpeg',
        title: 'image 1',
      },
      {
        image: '/images/properties/bedroom-8.jpeg',
        title: 'image 2',
      },
      {
        image: '/images/properties/bedroom-1.jpeg',
        title: 'image 3',
      },
    ],
    title: 'Best property in Albama',
    address: '2972, Washington Road, New Jersey',
    bedrooms: 3,
    bathrooms: 3,
    area: '1400 m2',
    owner: { id: 1, name: 'John Nash' },
    publishedDate: 'June 23, 2020',
    availability: 'rent',
    isTrending: false,
    price: '$670,500',
    pricePerSqFt: '$587/sqft',
    category: 'albama',
  },
  {
    id: 4,
    images: [
      {
        image: '/images/properties/living-room-1.jpeg',
        title: 'image 1',
      },
      {
        image: '/images/properties/living-room-2.jpeg',
        title: 'image 2',
      },
      {
        image: '/images/properties/living-room-3.jpeg',
        title: 'image 3',
      },
    ],
    title: 'Best house deal in New jersey',
    address: '2972, Washington Road, New Jersey',
    bedrooms: 3,
    bathrooms: 3,
    area: '1400 m2',
    owner: { id: 1, name: 'John Nash' },
    publishedDate: 'June 24, 2020',
    availability: 'sale',
    isTrending: false,
    price: '$670,500',
    pricePerSqFt: '$587/sqft',
    category: 'new_jersey',
  },
  {
    id: 5,
    images: [
      {
        image: '/images/properties/living-room-4.jpeg',
        title: 'image 1',
      },
      {
        image: '/images/properties/living-room-5.jpeg',
        title: 'image 2',
      },
      {
        image: '/images/properties/living-room-6.jpeg',
        title: 'image 3',
      },
    ],
    title: 'Luxury apartment in Colarado',
    address: '2972, Washington Road, New Jersey',
    bedrooms: 3,
    bathrooms: 3,
    area: '1400 m2',
    owner: { id: 1, name: 'John Nash' },
    publishedDate: 'June 28, 2020',
    availability: 'rent',
    isTrending: true,
    price: '$670,500',
    pricePerSqFt: '$587/sqft',
    category: 'colorado',
  },
  {
    id: 6,
    images: [
      {
        image: '/images/properties/living-room-7.jpeg',
        title: 'image 1',
      },
      {
        image: '/images/properties/living-room-8.jpeg',
        title: 'image 2',
      },
      {
        image: '/images/properties/living-room-1.jpeg',
        title: 'image 3',
      },
    ],
    title: 'Plot in Albama',
    address: '2972, Washington Road, New Jersey',
    bedrooms: 3,
    bathrooms: 3,
    area: '1400 m2',
    owner: { id: 1, name: 'John Nash' },
    publishedDate: 'June 29, 2020',
    availability: 'sale',
    isTrending: true,
    price: '$670,500',
    pricePerSqFt: '$587/sqft',
    category: 'albama',
  },
  {
    id: 7,
    images: [
      {
        image: '/images/properties/property-1.jpeg',
        title: 'image 1',
      },
      {
        image: '/images/properties/property-2.jpeg',
        title: 'image 2',
      },
      {
        image: '/images/properties/property-3.jpeg',
        title: 'image 3',
      },
    ],
    title: 'House in New jersey',
    address: '2972, Washington Road, New Jersey',
    bedrooms: 3,
    bathrooms: 3,
    area: '1400 m2',
    owner: { id: 1, name: 'John Nash' },
    publishedDate: 'June 24, 2020',
    availability: 'sale',
    isTrending: false,
    price: '$670,500',
    pricePerSqFt: '$587/sqft',
    category: 'new_jersey',
  },
  {
    id: 8,
    images: [
      {
        image: '/images/properties/property-4.jpeg',
        title: 'image 1',
      },
      {
        image: '/images/properties/property-5.jpeg',
        title: 'image 2',
      },
      {
        image: '/images/properties/property-6.jpeg',
        title: 'image 3',
      },
    ],
    title: 'Flat in Colarado',
    address: '2972, Washington Road, New Jersey',
    bedrooms: 3,
    bathrooms: 3,
    area: '1400 m2',
    owner: { id: 1, name: 'John Nash' },
    publishedDate: 'June 20, 2020',
    availability: 'rent',
    isTrending: true,
    price: '$670,500',
    pricePerSqFt: '$587/sqft',
    category: 'colorado',
  },
  {
    id: 9,
    images: [
      {
        image: '/images/properties/property-7.jpeg',
        title: 'image 1',
      },
      {
        image: '/images/properties/property-8.jpeg',
        title: 'image 2',
      },
      {
        image: '/images/properties/property-4.jpeg',
        title: 'image 3',
      },
    ],
    title: '3 BHK house in Albama',
    address: '2972, Washington Road, New Jersey',
    bedrooms: 3,
    bathrooms: 3,
    area: '1400 m2',
    owner: { id: 1, name: 'John Nash' },
    publishedDate: 'June 28, 2020',
    availability: 'sale',
    isTrending: false,
    price: '$670,500',
    pricePerSqFt: '$587/sqft',
    category: 'albama',
  },
  {
    id: 10,
    images: [
      {
        image: '/images/properties/property-5.jpeg',
        title: 'image 1',
      },
      {
        image: '/images/properties/bedroom-5.jpeg',
        title: 'image 2',
      },
      {
        image: '/images/properties/living-room-5.jpeg',
        title: 'image 3',
      },
    ],
    title: 'Best house for family in New Jersey',
    address: '2972, Washington Road, New Jersey',
    bedrooms: 3,
    bathrooms: 3,
    area: '1400 m2',
    owner: { id: 1, name: 'John Nash' },
    publishedDate: 'June 26, 2020',
    availability: 'rent',
    isTrending: true,
    price: '$670,500',
    pricePerSqFt: '$587/sqft',
    category: 'new_jersey',
  },
  {
    id: 11,
    images: [
      {
        image: '/images/properties/living-room-3.jpeg',
        title: 'image 1',
      },
      {
        image: '/images/properties/property-3.jpeg',
        title: 'image 2',
      },
      {
        image: '/images/properties/bedroom-3.jpeg',
        title: 'image 3',
      },
    ],
    title: 'Villa in Colarado',
    address: '2972, Washington Road, New Jersey',
    bedrooms: 3,
    bathrooms: 3,
    area: '1400 m2',
    owner: { id: 1, name: 'John Nash' },
    publishedDate: 'June 16, 2020',
    availability: 'rent',
    isTrending: true,
    price: '$670,500',
    pricePerSqFt: '$587/sqft',
    category: 'colorado',
  },
  {
    id: 12,
    images: [
      {
        image: '/images/properties/bedroom-8.jpeg',
        title: 'image 1',
      },
      {
        image: '/images/properties/living-room-8.jpeg',
        title: 'image 2',
      },
      {
        image: '/images/properties/property-8.jpeg',
        title: 'image 3',
      },
    ],
    title: 'Sunrise view apartment in Albama',
    address: '2972, Washington Road, New Jersey',
    bedrooms: 3,
    bathrooms: 3,
    area: '1400 m2',
    owner: { id: 1, name: 'John Nash' },
    publishedDate: 'June 28, 2020',
    availability: 'sale',
    isTrending: false,
    price: '$670,500',
    pricePerSqFt: '$587/sqft',
    category: 'albama',
  },
];

export const recentActivities = [
  {
    id: 1,
    date: getCustomDateTime(0, 'days', 'MMM DD, YYYY'),
    user: {
      id: 12,
      name: 'Alex Dolgove',
      profilePic: '/images/avatar/alex-dolgove.png',
    },
    mediaList: [
      {
        id: 123,
        name: 'Media1',
        mediaUrl: '/images/properties/bedroom-1.jpeg',
      },
      {
        id: 124,
        name: 'Media2',
        mediaUrl: '/images/properties/bedroom-2.jpeg',
      },
      {
        id: 125,
        name: 'Media3',
        mediaUrl: '/images/properties/bedroom-3.jpeg',
      },
    ],
    content: [
      <Box component="span" color="primary.main" className="pointer" mr={1}>
        Alex Dolgove
      </Box>,
      'left a 5 star review on Albama’s House',
    ],
  },
  {
    id: 2,
    date: getCustomDateTime(0, 'days', 'MMM DD, YYYY'),
    user: {
      id: 12,
      name: 'Kailasha',
      profilePic: '',
    },
    mediaList: [],
    content: [
      <Box component="span" color="primary.main" className="pointer" mr={1}>
        Kailasha
      </Box>,
      'is looking for a house in New Jersey, USA',
    ],
  },
  {
    id: 3,
    date: getCustomDateTime(0, 'days', 'MMM DD, YYYY'),
    user: {
      id: 12,
      name: 'Chelsea Johns',
      profilePic: '/images/avatar/chelsea-johns.jpg',
    },
    mediaList: [],
    content: [
      'Agent ',
      <Box component="span" color="primary.main" className="pointer" mr={1}>
        Chelsea Johns
      </Box>,
      'has added 7 new photos to the property ',
      <Box component="span" color="primary.main">
        Albama's house
      </Box>,
    ],
  },
  {
    id: 4,
    date: getCustomDateTime(-1, 'days', 'MMM DD, YYYY'),
    user: {
      id: 12,
      name: 'Domnic Brown',
      profilePic: '/images/avatar/domnic-brown.png',
    },
    mediaList: [
      {
        id: 123,
        name: 'Media1',
        mediaUrl: '/images/properties/bedroom-4.jpeg',
      },
      {
        id: 124,
        name: 'Media1',
        mediaUrl: '/images/properties/bedroom-5.jpeg',
      },
      {
        id: 125,
        name: 'Media1',
        mediaUrl: '/images/properties/bedroom-6.jpeg',
      },
    ],
    content: [
      'Welcome to a new agent ',
      <Box component="span" color="primary.main" className="pointer" mr={1}>
        Domnic Brown
      </Box>,
      'in the company.',
    ],
  },
  {
    id: 5,
    date: getCustomDateTime(-1, 'days', 'MMM DD, YYYY'),
    user: {
      id: 12,
      name: 'Michael Dogov',
      profilePic: '/images/avatar/michael-dogov.jpg',
    },
    mediaList: [],
    content: [
      <Box component="span" color="primary.main" className="pointer" mr={1}>
        Michael Dogov
      </Box>,
      'is looking for an office space in Colarado, USA.',
    ],
  },
  {
    id: 6,
    date: getCustomDateTime(-2, 'days', 'MMM DD, YYYY'),
    user: {
      id: 12,
      name: 'Domnic Harris',
      profilePic: '/images/avatar/domnic-harris.jpg',
    },
    mediaList: [],
    content: [
      <Box component="span" color="primary.main" className="pointer" mr={1}>
        Domnic Harris
      </Box>,
      "left a 5 star rating on Albama's property.",
    ],
  },
  {
    id: 7,
    date: getCustomDateTime(-2, 'days', 'MMM DD, YYYY'),
    user: {
      id: 12,
      name: 'Garry Sobars',
      profilePic: '/images/avatar/garry-sobars.jpg',
    },
    mediaList: [],
    content: [
      ' Callback request from ',
      <Box component="span" color="primary.main" className="pointer" mr={1}>
        Garry Sobars
      </Box>,
      'for the property ',
      <Box component="span" color="primary.main" className="pointer">
        Dmitri house
      </Box>,
    ],
  },
  {
    id: 8,
    date: getCustomDateTime(0, 'days', 'MMM DD, YYYY'),
    user: {
      id: 12,
      name: 'Guptil Sharma',
      profilePic: '/images/avatar/guptil.jpg',
    },
    mediaList: [
      {
        id: 123,
        name: 'Media1',
        mediaUrl: '/images/dashboard/userPhoto2.png',
      },
      {
        id: 124,
        name: 'Media2',
        mediaUrl: '/images/dashboard/userPhoto6.png',
      },
      {
        id: 125,
        name: 'Media3',
        mediaUrl: '/images/dashboard/userPhoto4.png',
      },
    ],
    content: [
      <Box component="span" color="primary.main" className="pointer" mr={1}>
        Guptil Sharma
      </Box>,
      "left a 5 star rating on Aloboma's house",
    ],
  },
  {
    id: 9,
    date: getCustomDateTime(0, 'days', 'MMM DD, YYYY'),
    user: {
      id: 12,
      name: 'Jeson Born',
      profilePic: '/images/avatar/jeson-born.jpg',
    },
    mediaList: [],
    content: [
      <Box component="span" color="primary.main" className="pointer" mr={1}>
        Jeson Born
      </Box>,
      'is looking for a house in New jersey, USA.',
    ],
  },
  {
    id: 10,
    date: getCustomDateTime(0, 'days', 'MMM DD, YYYY'),
    user: {
      id: 12,
      name: 'Jimmy Jo',
      profilePic: '/images/avatar/jimmy-jo.jpg',
    },
    mediaList: [],
    content: [
      'Agent ',
      <Box component="span" color="primary.main" className="pointer" mr={1}>
        Jimmy Jo
      </Box>,
      'has added 7 new photos to the property ',
      <Box component="span" color="primary.main">
        Albama's house
      </Box>,
    ],
  },
  {
    id: 11,
    date: getCustomDateTime(-1, 'days', 'MMM DD, YYYY'),
    user: {
      id: 12,
      name: 'Jonathan Lee',
      profilePic: '/images/avatar/jonathan.jpg',
    },
    mediaList: [
      {
        id: 123,
        name: 'Media1',
        mediaUrl: '/images/dashboard/userPhoto6.png',
      },
      {
        id: 124,
        name: 'Media1',
        mediaUrl: '/images/dashboard/userPhoto1.png',
      },
      {
        id: 125,
        name: 'Media1',
        mediaUrl: '/images/dashboard/userPhoto3.png',
      },
    ],
    content: [
      'Welcome to a new agent ',
      <Box component="span" color="primary.main" className="pointer" mr={1}>
        Jonathan Lee
      </Box>,
      'in the company.',
    ],
  },
  {
    id: 12,
    date: getCustomDateTime(-1, 'days', 'MMM DD, YYYY'),
    user: {
      id: 12,
      name: 'Joshua',
      profilePic: '/images/avatar/joshua.jpg',
    },
    mediaList: [],
    content: [
      <Box component="span" color="primary.main" className="pointer" mr={1}>
        Joshua
      </Box>,
      'is looking for an office space in Colarado, USA.',
    ],
  },
  {
    id: 13,
    date: getCustomDateTime(-2, 'days', 'MMM DD, YYYY'),
    user: {
      id: 12,
      name: 'Stella Johnson',
      profilePic: '/images/avatar/stella-johnson.png',
    },
    mediaList: [],
    content: [
      <Box component="span" color="primary.main" className="pointer" mr={1}>
        Stella Johnson
      </Box>,
      "left a 5 star rating on Albama's property.",
    ],
  },
];

export const newCustomers = [
  {
    id: 1,
    name: 'Albert Hall',
    profilePic: '/images/avatar/avatar6.jpg',
  },
  {
    id: 2,
    name: 'John Hall',
    profilePic: '/images/avatar/avatar7.jpg',
  },
  { id: 3, name: 'Jackson Hall', profilePic: '' },
  {
    id: 4,
    name: 'Jonty Hall',
    profilePic: '/images/avatar/avatar3.jpg',
  },
  {
    id: 5,
    name: 'Jonathan Hall',
    profilePic: '/images/avatar/avatar4.jpg',
  },
  {
    id: 6,
    name: 'Shane Hall',
    profilePic: '/images/avatar/avatar5.jpg',
  },
  {
    id: 7,
    name: 'Lisa Hall',
    profilePic: '/images/avatar/avatar3.jpg',
  },
  { id: 8, name: 'Cheeni Hall', profilePic: '' },
  {
    id: 9,
    name: 'Chilbram Hall',
    profilePic: '/images/avatar/avatar4.jpg',
  },
  {
    id: 10,
    name: 'Danny Hall',
    profilePic: '/images/avatar/avatar5.jpg',
  },
];
