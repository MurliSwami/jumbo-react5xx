import { getCustomDateTime } from '../../@jumbo/utils/dateHelper';

export const user = {
  id: 3432434,
  name: 'Kiley Brown',
  cover_pic: '/images/cover_pic.png',
  username: 'kiley',
  location: 'Florida, USA',
  profile_pic: '/images/avatar/avatar6.jpg',
  followers: 2342,
  following: 47,
  email: 'domnicharris@example.com',
  website: 'www.domnic.com',
  phone: '+1-987(454)987',
  friends: {
    total: 327,
    frndProfiles: [
      {
        id: 4523,
        profile_pic: '/images/avatar/avatar7.jpg',
        name: 'Svetlana Harris',
        status: 'online',
      },
      {
        id: 5454,
        profile_pic: '/images/avatar/avatar8.jpg',
        name: 'Mark Taylor',
        status: 'offline',
      },
      {
        id: 3434,
        profile_pic: '/images/avatar/avatar9.jpg',
        name: 'Maxmilian',
        status: 'away',
      },
      {
        id: 3455,
        profile_pic: '/images/avatar/avatar10.jpg',
        name: 'Jonas Milian',
        status: 'away',
      },
      {
        id: 5634,
        profile_pic: '/images/avatar/avatar11.jpg',
        name: 'Josh Brake',
        status: 'offline',
      },
    ],
    mutual: 27,
  },
  pictures: [
    {
      id: 123,
      url: '/images/avatar/avatar14.jpg',
      size: '1.2 mb',
      name: 'Aunt',
    },
    {
      id: 456,
      url: '/images/avatar/avatar4.jpg',
      size: '2.2 mb',
      name: 'Doctor',
    },
    {
      id: 456,
      url: '/images/avatar/avatar5.jpg',
      size: '4.2 mb',
      name: 'Candid',
    },
    {
      id: 76456,
      url: '/images/products/product1.png',
      size: '1.9 mb',
      name: 'Watch',
    },
    {
      id: 456,
      url: '/images/avatar/avatar6.jpg',
      size: '1.6 mb',
      name: 'Me',
    },
    {
      id: 456,
      url: '/images/avatar/avatar12.jpg',
      size: '1.4 mb',
      name: 'Candid',
    },
  ],
  company: 'G-axon Tech Pvt. Ltd.',
  birthday: 'Oct 25, 1994',
  college: 'Oxford University',
  locality: 'Switzerland',
  family: [
    {
      id: 4343,
      name: 'John',
      profile_pic: '/images/avatar/avatar3.jpg',
    },
    {
      id: 5454,
      name: 'Dhruva Sharma',
      profile_pic: '/images/avatar/avatar4.jpg',
    },
    {
      id: 8965,
      name: 'Lily Taylor',
      profile_pic: '/images/avatar/avatar5.jpg',
    },
    {
      id: 3457,
      name: 'Josh',
      profile_pic: '/images/avatar/avatar6.jpg',
    },
  ],
  events: [
    {
      id: 123,
      type: 'Musical Concert',
      title: 'Sundance Film Festival',
      location: 'Downsview Park, Toronto, Canada',
      date: 'Feb 23, 2020',
      thumb: '/images/event1.jpg',
    },
    {
      id: 433,
      type: 'Magic Show',
      title: 'Underwater Musical Festival',
      location: 'Downsview Park, Toronto, Canada',
      date: 'Feb 11, 2020',
      thumb: '/images/event2.jpg',
    },
    {
      id: 654,
      type: 'Musical Concert',
      title: 'Village Feast Fac',
      location: 'Downsview Park, Toronto, Canada',
      date: 'Jan 02, 2020',
      thumb: '/images/event3.jpg',
    },
  ],
  interests: ['Music', 'Graphic Design', 'Drawing', 'Sports', 'Tourism'],
  communities: [
    {
      id: 33232,
      name: 'Friends',
      thumb: '/images/wall/community1.png',
    },
    {
      id: 45435,
      name: 'Event',
      thumb: '/images/wall/community2.png',
    },
    {
      id: 43323,
      name: 'Birthday',
      thumb: '/images/wall/community3.png',
    },
    {
      id: 78565,
      name: 'Family',
      thumb: '/images/wall/community4.png',
    },
    {
      id: 87967,
      name: 'Work',
      thumb: '/images/wall/community5.png',
    },
    {
      id: 90777,
      name: 'Other',
      thumb: '/images/wall/community6.png',
    },
  ],
};

export const postsList = [
  {
    id: 123,
    owner: {
      name: 'Kiley Brown',
      profile_pic: '/images/avatar/avatar6.jpg',
      id: 3432434,
    },
    date: getCustomDateTime(-1, 'days', 'ddd MMM DD YYYY kk:mm:ss Z'),
    attachments: [
      {
        id: 123,
        path: '/images/wall/attachment1.png',
        preview: '/images/wall/attachment1.png',
        metaData: { type: 'images/jpg', size: 4543 },
      },
      {
        id: 455,
        path: '/images/wall/attachment2.png',
        preview: '/images/wall/attachment2.png',
        metaData: { type: 'images/jpg', size: 2345 },
      },
    ],
    content: 'Nature beauty at its best.',
    liked: true,
    likes: 345,
    shares: 124,
    views: 12,
    comments: [
      {
        id: 21,
        owner: {
          name: 'James Jennie',
          profile_pic: '/images/avatar/avatar7.jpg',
          id: 343432,
        },
        liked: true,
        comment: 'Wow! Excellent, these images are so beautiful.',
        date: getCustomDateTime(-1, 'days', 'ddd MMM DD YYYY kk:mm:ss Z'),
      },
    ],
  },
  {
    id: 4353,
    owner: {
      name: 'Sara Taylor',
      profile_pic: '/images/avatar/avatar8.jpg',
      id: 344343,
    },
    date: getCustomDateTime(-2, 'days', 'ddd MMM DD YYYY kk:mm:ss Z'),
    attachments: [
      {
        id: 544,
        path: '/images/wall/attachment3.png',
        preview: '/images/wall/attachment3.png',
        metaData: { type: 'images/jpg', size: 4056 },
      },
      {
        id: 676,
        path: '/images/wall/attachment4.png',
        preview: '/images/wall/attachment4.png',
        metaData: { type: 'images/jpg', size: 4056 },
      },
      {
        id: 545,
        path: '/images/wall/attachment5.png',
        preview: '/images/wall/attachment5.png',
        metaData: { type: 'images/jpg', size: 4056 },
      },
    ],
    content: 'Nature beauty at its best.',
    liked: false,
    likes: 4545,
    shares: 334,
    views: 54654,
    comments: [
      {
        id: 5465,
        owner: {
          name: 'James Jennie',
          profile_pic: '/images/avatar/avatar7.jpg',
          id: 343432,
        },
        liked: true,
        comment: 'Wow! Excellent, these images are so beautiful.',
        date: getCustomDateTime(0, 'days', 'ddd MMM DD YYYY kk:mm:ss Z'),
      },
    ],
  },
  {
    id: 4354,
    owner: {
      name: 'Domnic Harris',
      profile_pic: '/images/avatar/avatar5.jpg',
      id: 3432434,
    },
    date: getCustomDateTime(-3, 'days', 'ddd MMM DD YYYY kk:mm:ss Z'),
    attachments: [
      {
        id: 5677,
        path: '/images/wall/attachment6.png',
        preview: '/images/wall/attachment6.png',
        metaData: { type: 'images/jpg', size: 4056 },
      },
    ],
    content: '',
    liked: true,
    likes: 454,
    shares: 67,
    views: 5465,
    comments: [],
  },
];
