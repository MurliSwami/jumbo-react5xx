import React from 'react';
import { CardActions, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: props => ({
    position: 'relative',
    zIndex: 1,
    padding: '8px 24px',
  }),
}));

const CmtCardActions = ({ children, ...rest }) => {
  const classes = useStyles();

  return (
    <CardActions className={classes.root} {...rest}>
      {children}
    </CardActions>
  );
};

export default CmtCardActions;
