import React from 'react';
import { CardActionArea, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: props => ({
    position: 'relative',
    zIndex: 1,
  }),
}));

const CmtCardActionArea = ({ children, ...rest }) => {
  const classes = useStyles();
  return (
    <CardActionArea className={classes.root} {...rest}>
      {children}
    </CardActionArea>
  );
};

export default CmtCardActionArea;
