import React from 'react';
import GridView from './GridView';
import PropTypes from 'prop-types';
import GridFooter from './GridFooter';

const CmtGridView = ({
  itemPadding,
  containerStyle,
  ListEmptyComponent,
  data,
  onEndReached,
  renderRow,
  footerProps,
  ...rest
}) => {
  return (
    <GridView
      {...{
        itemPadding,
        containerStyle,
        ListEmptyComponent,
        data,
        onEndReached,
        renderRow,
      }}
      {...rest}
      ListFooterComponent={
        footerProps ? <GridFooter loading={footerProps.loading} footerText={footerProps.footerText} /> : null
      }
    />
  );
};

export default CmtGridView;
CmtGridView.propTypes = {
  itemPadding: PropTypes.number,
  containerStyle: PropTypes.object,
  ListEmptyComponent: PropTypes.node,
  ListFooterComponent: PropTypes.node,
  data: PropTypes.array.isRequired,
  onEndReached: PropTypes.func,
  renderRow: PropTypes.func,
};
CmtGridView.defaultProps = {
  itemPadding: 0,
  data: [],
};
