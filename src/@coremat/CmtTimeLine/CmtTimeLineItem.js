import React from 'react';
import useStyles from './CmtTimeLineItem.style';
import clsx from 'clsx';
import Box from '@material-ui/core/Box';
import PropTypes from 'prop-types';

const colors = ['#ff0000', '#ff0000', '#ff0000', '#ff0000', '#ff0000'];

const CmtTimeLineItem = ({ point, pointColor, head, content }) => {
  const makeDot = () => {
    return <Box component="span" className={clsx(classes.makeDot, 'Cmt-make-dot')} style={{ backgroundColor: colors[1] }} />;
  };
  const classes = useStyles();
  const pointColorStyle = pointColor ? { backgroundColor: pointColor } : {};
  return (
    <Box className={clsx(classes.timelineItem, 'Cmt-timeline-item')}>
      {point ? (
        <Box className={clsx(classes.timelineBadge, 'Cmt-timeline-badge')} style={pointColorStyle}>
          {point}
        </Box>
      ) : (
        makeDot()
      )}
      {content}
      {head ? <Box className={clsx(classes.timelineTime, 'Cmt-timeline-time')}>{head}</Box> : null}
    </Box>
  );
};

CmtTimeLineItem.propTypes = {
  point: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  pointColor: PropTypes.string,
  head: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  content: PropTypes.oneOfType([PropTypes.string, PropTypes.element]).isRequired,
};

export default CmtTimeLineItem;
