import React from 'react';
import ListView from './ListView';
import PropTypes from 'prop-types';
import ListFooter from './ListFooter';

const CmtList = ({ footerProps, ...props }) => {
  return (
    <ListView
      {...props}
      ListFooterComponent={
        footerProps ? <ListFooter loading={footerProps.loading} footerText={footerProps.footerText} /> : null
      }
    />
  );
};

export default CmtList;
CmtList.propTypes = {
  containerStyle: PropTypes.object,
  ListEmptyComponent: PropTypes.node,
  ListFooterComponent: PropTypes.node,
  data: PropTypes.array.isRequired,
  onEndReached: PropTypes.func,
};
CmtList.defaultProps = {
  data: [],
};
