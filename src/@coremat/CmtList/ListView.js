import React from 'react';
import { useBottomScrollListener } from 'react-bottom-scroll-listener';
import PropTypes from 'prop-types';
import { Box } from '@material-ui/core';

const getEmptyContainer = ListEmptyComponent => {
  if (ListEmptyComponent) return React.isValidElement(ListEmptyComponent) ? ListEmptyComponent : <ListEmptyComponent />;
  return null;
};

const getFooterContainer = ListFooterComponent => {
  if (ListFooterComponent) return React.isValidElement(ListFooterComponent) ? ListFooterComponent : <ListFooterComponent />;
  return null;
};
const ListView = ({
  renderRow,
  onEndReached,
  data,
  containerStyle,
  ListFooterComponent,
  ListEmptyComponent,
  ItemSeparatorComponent,
  ...rest
}) => {
  if (!onEndReached) {
    onEndReached = () => {};
  }

  useBottomScrollListener(onEndReached, 200);
  return (
    <Box style={containerStyle} {...rest}>
      {data.length > 0 ? data.map((item, index) => renderRow(item, index)) : getEmptyContainer(ListEmptyComponent)}
      {getFooterContainer(ListFooterComponent)}
    </Box>
  );
};

export default ListView;
ListView.propTypes = {
  containerStyle: PropTypes.object,
  ListEmptyComponent: PropTypes.node,
  ListFooterComponent: PropTypes.node,
  data: PropTypes.array.isRequired,
  onEndReached: PropTypes.func,
  renderRow: PropTypes.func,
};
ListView.defaultProps = {
  data: [],
  onEndReached: () => {},
};
