import React, { useContext } from 'react';
import Box from '@material-ui/core/Box';
import CmtGridView from '../../../../../@coremat/CmtGridView';
import CmtImage from '../../../../../@coremat/CmtImage';
import CmtCard from '../../../../../@coremat/CmtCard';
import CmtCardHeader from '../../../../../@coremat/CmtCard/CmtCardHeader';
import CmtCardContent from '../../../../../@coremat/CmtCard/CmtCardContent';
import { makeStyles } from '@material-ui/core';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import AppContext from '../../../contextProvider/AppContextProvider/AppContext';
import { DRAWER_BREAK_POINT, HEADER_TYPE, LAYOUT_TYPES, SIDEBAR_TYPE } from '../../../../constants/ThemeOptions';

const layoutList = [
  {
    id: LAYOUT_TYPES.VERTICAL_DEFAULT,
    layoutName: 'Default',
    image: '/images/layouts/layout1.png',
  },
  {
    id: LAYOUT_TYPES.VERTICAL_MINIMAL,
    layoutName: 'Minimal',
    image: '/images/layouts/layout2.png',
  },
  {
    id: LAYOUT_TYPES.VERTICAL_MINIMAL_NO_HEADER,
    layoutName: 'Minimal No Header',
    image: '/images/layouts/layout3.png',
  },
  {
    id: LAYOUT_TYPES.HORIZONTAL_DEFAULT,
    layoutName: 'Horizontal Default',
    image: '/images/layouts/layout4.png',
  },
  {
    id: LAYOUT_TYPES.HORIZONTAL_DARK,
    layoutName: 'Horizontal Dark Header',
    image: '/images/layouts/layout5.png',
  },
  {
    id: LAYOUT_TYPES.HORIZONTAL_MINIMAL,
    layoutName: 'Horizontal Minimal',
    image: '/images/layouts/layout6.png',
  },
  {
    id: LAYOUT_TYPES.HORIZONTAL_TOP_MENU,
    layoutName: 'Horizontal Top Bar',
    image: '/images/layouts/layout7.png',
  },
  {
    id: LAYOUT_TYPES.VERTICAL_MODERN_SIDEBAR,
    layoutName: 'Modern',
    image: '/images/layouts/layout8.png',
  },
];

const useStyles = makeStyles(theme => ({
  cardRoot: {
    '& .Cmt-header-root': {
      padding: 16,
    },
    '& .Cmt-card-content': {
      paddingLeft: 16,
      paddingRight: 16,
      paddingBottom: 16,
    },
  },
  checkIcon: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    fill: theme.palette.success.main,
  },
}));

const NavigationLayout = () => {
  const classes = useStyles();
  const { layout, updateLayout } = useContext(AppContext);

  const onSelectLayout = layoutType => {
    switch (layoutType) {
      case LAYOUT_TYPES.VERTICAL_DEFAULT: {
        return updateLayout(layoutType, HEADER_TYPE.FIXED, DRAWER_BREAK_POINT.MD, SIDEBAR_TYPE.FULL);
      }
      case LAYOUT_TYPES.VERTICAL_MINIMAL: {
        return updateLayout(layoutType, HEADER_TYPE.FIXED, DRAWER_BREAK_POINT.SM, SIDEBAR_TYPE.MINI);
      }
      case LAYOUT_TYPES.VERTICAL_MINIMAL_NO_HEADER: {
        return updateLayout(layoutType, HEADER_TYPE.FIXED, DRAWER_BREAK_POINT.SM, SIDEBAR_TYPE.MINI);
      }
      case LAYOUT_TYPES.VERTICAL_MODERN_SIDEBAR: {
        return updateLayout(layoutType, HEADER_TYPE.FIXED, DRAWER_BREAK_POINT.MD, SIDEBAR_TYPE.MINI);
      }
      default:
        updateLayout(layoutType, HEADER_TYPE.FIXED, DRAWER_BREAK_POINT.MD, SIDEBAR_TYPE.FULL);
    }
  };

  return (
    <CmtCard className={classes.cardRoot}>
      <CmtCardHeader title="NavigationLayout" />
      <CmtCardContent>
        <CmtGridView
          itemPadding={16}
          data={layoutList}
          renderRow={(item, index) => (
            <Box key={index} className="pointer" onClick={() => onSelectLayout(item.id)}>
              <Box position="relative">
                <CmtImage src={item.image} alt={item.layoutName} />
                {item.id === layout && <CheckCircleIcon className={classes.checkIcon} />}
              </Box>
              <Box mt={2}>{item.layoutName}</Box>
            </Box>
          )}
        />
      </CmtCardContent>
    </CmtCard>
  );
};

export default NavigationLayout;
