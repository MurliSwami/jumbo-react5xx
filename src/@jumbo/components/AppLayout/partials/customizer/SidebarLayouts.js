import React, { useContext } from 'react';
import { SIDEBAR_TYPE } from '../../../../constants/ThemeOptions';
import { makeStyles } from '@material-ui/core';
import CmtCard from '../../../../../@coremat/CmtCard';
import CmtCardHeader from '../../../../../@coremat/CmtCard/CmtCardHeader';
import CmtCardContent from '../../../../../@coremat/CmtCard/CmtCardContent';
import CmtGridView from '../../../../../@coremat/CmtGridView';
import Box from '@material-ui/core/Box';
import CmtImage from '../../../../../@coremat/CmtImage';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import AppContext from '../../../contextProvider/AppContextProvider/AppContext';

const useStyles = makeStyles(theme => ({
  cardRoot: {
    '& .Cmt-header-root': {
      padding: 16,
    },
    '& .Cmt-card-content': {
      padding: 16,
    },
  },
  checkIcon: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    fill: theme.palette.success.main,
  },
}));

const layoutList = [
  {
    id: SIDEBAR_TYPE.MINI,
    layoutName: 'Folded',
    image: '/images/customizer/sidebar/layouts/folded.png',
  },
  {
    id: SIDEBAR_TYPE.FULL,
    layoutName: 'Default',
    image: '/images/customizer/sidebar/layouts/default.png',
  },
  {
    id: SIDEBAR_TYPE.DRAWER,
    layoutName: 'Drawer',
    image: '/images/customizer/sidebar/layouts/drawer.png',
  },
];

const SidebarLayouts = () => {
  const classes = useStyles();
  const { sidebarType, updateSidebarType } = useContext(AppContext);

  return (
    <CmtCard className={classes.cardRoot}>
      <CmtCardHeader title="Sidebar Layouts" />
      <CmtCardContent>
        <CmtGridView
          itemPadding={16}
          data={layoutList}
          renderRow={(item, index) => (
            <Box key={index} className="pointer" onClick={() => updateSidebarType(item.id)}>
              <Box position="relative">
                <CmtImage src={item.image} alt={item.layoutName} />
                {item.id === sidebarType && <CheckCircleIcon className={classes.checkIcon} />}
              </Box>
              <Box mt={2}>{item.layoutName}</Box>
            </Box>
          )}
        />
      </CmtCardContent>
    </CmtCard>
  );
};

export default SidebarLayouts;
